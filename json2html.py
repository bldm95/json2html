import html
import json
import re
import sys
from typing import Union

JSON_FILE = "source.json"


class HtmlConverter(object):
    def __init__(self):
        self.elements = list()

    def convert(self) -> str:
        result = ""
        for element in self.elements:
            result += element.render()
        return result

    def load_tags_from_json(self, json_data: Union[list, dict]) -> "HtmlConverter":
        if isinstance(json_data, list):
            self.elements.append(
                BlockTagContainer.get_block_tag_container(json_data)
            )
        else:
            for html_tag, content in json_data.items():
                if isinstance(content, list):
                    self.elements.append(
                        BlockTagContainer.get_block_tag_container(
                            content, html_tag)
                    )
                else:
                    self.elements.append(Tag(html_tag, content).get_tag())

        return self


class Tag(object):
    def __init__(self, tag_with_selectors: str, content: Union[str, list, object, bool, int, float, None]):
        self.tag_with_selectors = tag_with_selectors
        self.content = content
        self.tag_name = ""
        self.list_class = ""
        self.id = ""

    def get_tag_name(self) -> str:
        index_start_class_name = self.tag_with_selectors.find('.')
        if index_start_class_name > -1:
            return self.tag_with_selectors[:index_start_class_name]
        elif index_start_class_name == -1:
            index_start_id = self.tag_with_selectors.find('#')
            if index_start_id > -1:
                return self.tag_with_selectors[:index_start_id]
            else:
                return self.tag_with_selectors

    def get_id(self) -> str:
        index_start_id = self.tag_with_selectors.find('#')
        if index_start_id > -1:
            return "id=\"{}\"".format(self.tag_with_selectors[index_start_id + 1:])
        return ""

    def get_list_class(self) -> str:
        list_class = re.findall(r'\.(.+?\b(?:-.+?\b)?)', self.tag_with_selectors)
        if list_class:
            return "class=\"" + " ".join(list_class) + "\""
        return ""

    def get_tag(self):
        self.tag_name = self.get_tag_name()
        self.id = self.get_id()
        self.list_class = self.get_list_class()
        return self

    def render(self) -> str:
        return "<{}{}{}{}{}>{}</{}>".format(
            self.tag_name,
            " "*bool(self.list_class), self.list_class,
            " "*bool(self.id), self.id,
            html.escape(self.content),
            self.tag_name)


class TagContainer(object):
    def __init__(self):
        self.list_tags = list()

    @staticmethod
    def get_tag_container(json_data: dict) -> object:
        tag_container = TagContainer()

        for html_tag, content in json_data.items():
            if isinstance(content, list):
                tag_container.list_tags.append(
                    BlockTagContainer.get_block_tag_container(content, html_tag)
                )
            else:
                tag_container.list_tags.append(Tag(html_tag, content).get_tag())
        return tag_container

    def render(self) -> str:
        result = "<li>"
        for tag in self.list_tags:
            result += tag.render()
        result += "</li>"
        return result


class BlockTagContainer(object):
    def __init__(self, html_tag: str):
        self.html_tag = html_tag
        self.list_tag_container: list = list()

    @staticmethod
    def get_block_tag_container(json_data: Union[list, dict], list_tag_container_html_tag: str = "") -> object:
        block_tag_container = BlockTagContainer(list_tag_container_html_tag)
        for el in json_data:
            if isinstance(el, list):
                block_tag_container.list_tag_container.append(
                    BlockTagContainer.get_block_tag_container(el)
                )
            else:
                block_tag_container.list_tag_container.append(
                    TagContainer.get_tag_container(el)
                )
        return block_tag_container

    def render(self) -> str:
        result = ""
        if self.html_tag:
            result += "<{}>".format(self.html_tag)
        result += "<ul>"
        for tag_container in self.list_tag_container:
            result += tag_container.render()
        result += "</ul>"
        if self.html_tag:
            result += "</{}>".format(self.html_tag)
        return result


def load_data(file: str) -> Union[list, dict]:
    try:
        with open(file) as json_file:
            data = json.load(json_file)
            return data
    except FileNotFoundError:
        print("File not found error: please check {} in the directory".format(file))
    except IOError:
        print("An IOError has occurred!")
    except json.decoder.JSONDecodeError:
        print("Wrong JSON format in {}".format(file))
    sys.exit(1)


if __name__ == "__main__":
    sys.stdout.write(HtmlConverter().load_tags_from_json(load_data(JSON_FILE)).convert())
