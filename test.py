import unittest
from json2html import convert

VALID_JSON_DATA = [
    {
        "title": "Title #1",
        "body": "Hello, World 1!"
    },
    {
        "title": "Title #2",
        "body": "Hello, World 2!"
    }
]


class Json2HtmlTestCase(unittest.TestCase):
    def test_convert_invalid_JSON(self):
        with self.assertRaises(SystemExit) as cm:
            convert([{"head": "test_head"}])
        self.assertEqual(cm.exception.code, 1)

    def test_convert_valid_JSON(self):
        self.assertEqual(convert(VALID_JSON_DATA),
                         "<h1>Title #1</h1><p>Hello, World 1!</p><h1>Title #2</h1><p>Hello, World 2!</p>")


if __name__ == '__main__':
    unittest.main()
